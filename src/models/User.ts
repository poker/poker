/// <reference path="../../typings/node-uuid/node-uuid.d.ts" />
/// <reference path="../../typings/socket.io/socket.io.d.ts" />

import * as uuid from 'node-uuid';

import Player from '../poker/Player';

export default class User {

  id = uuid.v1();
  tableChips: number = 500;

  constructor(private socket: SocketIO.Socket, private name: string) {}

  getId() { return this.id; }
  getName() { return this.name; }
  getSocket() { return this.socket; }
  getTableChips() { return this.tableChips; }
  setTableChips(chips: number) { this.tableChips = chips; }
  addTableChips(chips: number) { this.tableChips += chips; }
}