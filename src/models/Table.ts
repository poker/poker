/// <reference path="../../typings/node-uuid/node-uuid.d.ts" />

import * as _ from 'underscore'
import * as uuid from 'node-uuid';

import Poker from '../poker/Poker';
import User from './User';
import * as events from '../poker/events';

export default class Table {

  id: string = uuid.v1();

  private users: User[] = [];
  private poker: Poker = new Poker([], 0, this.blind);

  constructor(private blind: number) {}

  addPlayer(user: User) { 
    this.users.push(user);
    this.poker.addPlayer(user.getTableChips(), this.users.length);
  }

  getId() { return this.id; }

  getUsers() { return this.users; }

  getBlind() { return this.blind; }

  getPoker() { return this.poker; }

  getCurrUser() {
    return this.users[this.poker.getCurrPlayerIndex()];
  }

  size() { return this.users.length; }
  
  broadcast(socket: SocketIO.Socket) { return socket.broadcast.to(this.id); }

  broadcastAll(io: SocketIO.Server) { return io.sockets.in(this.id); }
}
