/// <reference path="../../typings/node/node.d.ts" />

import * as _ from 'underscore';
import { EventEmitter } from 'events';

import Player from './Player';
import Deck from './Deck';
import Card from './Card';

import * as events from './events';

export default class Poker {

  private deck: Deck;
  private players: Player[];

  private currPlayerIndex: number;
  private currBet: number;
  private startRoundPlayer: number;
  private minRaise: number;

  private emitter = new EventEmitter();

  constructor(playersChip: number[], private button: number, private blind: number) {
    this.players = _(playersChip).map(chips => new Player(chips));
  }

  addPlayer(playerChips: number, index: number) {
    const player = new Player(playerChips);
    player.fold();
    this.players.splice(index, 0, player);
  }

  newHand() {
    if (this.players.length < 2) throw new Error('Must have 2 or more players');
     // new hand
    _(this.players).each(p => { p.newHand(); });

    // shuffle and deal cards
    this.deck = new Deck();
    this.deck.shuffle();
    _.times(2, () => {
      _(this.players).each(p => { p.addCard(this.deck.dealCard()); })
    });

    this.currPlayerIndex = this.button;
    this.minRaise = 0;
    this.currBet = 0;
    this.startRoundPlayer = -1;

    // small blind
    this.nextPlayer();
    this.getCurrPlayer().getChips() < this.blind / 2 ? this.allIn() : this.raise(this.blind / 2);

    this.getCurrPlayer().getChips() < this.blind ? this.allIn() : this.raise(this.blind / 2);

    this.newBettingRound();
    this.emitter.emit('NewHand', events.NewHand(this.players, this.button++, this.blind));
  }

  private bet(chips: number) {
    var player = this.getCurrPlayer(),
      toCallChips = this.getToCallChips();

    if (chips)
      player.bet(chips);
    else 
      player.fold();

    // update current bet
    if (this.currBet < player.getBet()) 
      this.currBet = player.getBet();

    this.nextPlayer();
  }

  on(event: string, callback: Function) {
    this.emitter.on(event, callback);
  }

  fold() {
    this.bet(0);
  }

  check() {
    if (this.isRoundFinsished()) throw new Error('Round is finished, cannot check');

    if (this.currBet > this.getCurrPlayer().getBet()) throw new Error('Cannot check');

    this.nextPlayer();
  }

  call() {
    var toCallChips = this.getToCallChips();
    
    if (toCallChips == 0) {
      this.check();
    } else if (toCallChips < this.getCurrPlayer().getChips()) {
      this.bet(toCallChips);
    } else {
      throw new Error('Cannot call, should be all-in.');
    }
  }

  raise(raiseChips: number) {
    var player = this.getCurrPlayer(),
      toCallChips = this.getToCallChips();

    if (toCallChips + raiseChips > player.getChips()) { throw new Error('Not enough money to raise'); }
    console.log(raiseChips, this.minRaise);
    if (raiseChips < this.minRaise) { throw new Error('Raise must be > minRaise'); }
    this.minRaise = raiseChips;

    this.startRoundPlayer = this.currPlayerIndex;

    this.bet(toCallChips + raiseChips);

    if (player.getChips() === 0) {
      this.startRoundPlayer = this.currPlayerIndex;
    }
  }

  allIn() {
    var toCallChips = this.getToCallChips(),
      playerChips = this.getCurrPlayer().getChips();
    if (playerChips < toCallChips) {
      this.bet(this.getCurrPlayer().getChips());
    } else if (playerChips == toCallChips) {
      this.call();
    } else {
      this.raise(playerChips - toCallChips);
    }
  }

  private newBettingRound() {
    this.minRaise = this.blind;
  }

  private dealCommonCards() {
    const len = this.getCardCount();
    if (len < 5) {
      this.newBettingRound();
      // deal card(s)
      this.deck.dealCard();
      _(len ? 3 : 1).times(() => {
        var card = this.deck.dealCard();
        _(this.players).each(player => player.addCard(card)); 
      });
    } else {
      throw new Error('There\'s nothing after river');
    }
  }

  getPlayers(): Player[] { return this.players; }

  getCurrPlayerIndex() { return this.currPlayerIndex; }
  getCurrPlayer(): Player { return this.players[this.currPlayerIndex]; }

  nextPlayer() {
    this.currPlayerIndex = (this.currPlayerIndex + 1) % this.players.length;
    if (
      !this.isRoundFinsished() && 
      (this.getCurrPlayer().isFolded() || this.getCurrPlayer().getChips() === 0)
    ) this.nextPlayer();

    if (this.isRoundFinsished()) {
      const activePlayers = this.getActivePlayers();
      if (activePlayers.length> 1) {
        this.dealCommonCards();
      } else {
        this.showDown();
      }
    }
  }

  isRoundFinsished() { return this.currPlayerIndex === this.startRoundPlayer; }

  getActivePlayers() { return _(this.players).filter(p => !p.isFolded() && p.getChips() > 0); }

  getCardCount() { return this.players[0].getHand().size() - 2;}

  getCurrBet(): number { return this.currBet; }

  getToCallChips(): number { return this.currBet - this.getCurrPlayer().getBet(); }

  getMinRaise(): number { return this.minRaise; }

  getBlind() { return this.blind; }

  showDown() { 
    const results = Poker.calculatePots(this.players);
    _(results).each(result => {
      result.player.addChips(result.winningChips);
    });
    this.emitter.emit('HandEnd', events.HandEnd(_(results).map(r => r.winningChips)));
  }

  static calculatePots(players: Player[]): PotResult[] {

    function calculate(sorted: PotResult[][]): PotResult[] {
      const results = _(sorted).flatten();

      if (_(results).every(result => result.bet <= 0)) 
        return results;

      const withBetSorted = _.chain(sorted)
        .map(group => _(group).filter(result => result.bet > 0))
        .filter(group => group.length > 0)
        .value();

      const highScoreGroup = withBetSorted[0],
        lowBetResult = highScoreGroup[0],
        bet = lowBetResult.bet;

      const potChips = _(results).reduce((sum, result: PotResult) => {
        return sum + _.min([bet, result.bet]);
      }, 0);

      const updateSorted = _(sorted).map((group: PotResult[], index) => {
        return _(group).map((result): PotResult => {
          const winningChips = _(highScoreGroup).contains(result) ? Math.floor(potChips / highScoreGroup.length) : 0;
          return {
            player: result.player,
            bet: _.max([result.bet - bet, 0]),
            winningChips: result.winningChips + winningChips
          };
        });
      });
      return calculate(updateSorted);
    }

    const sorted = _.chain(players)
      .groupBy(player => {
        const bestHand = player.getHand().getBestPokerHand();
        return player.isFolded() ? -2 : bestHand && bestHand.getScore() || -1;
      })
      .sortBy((group, score) => { return score * -1; })
      .map(group => {
        return _.chain(group)
          .map((player): PotResult => {
            return {
              player: player,
              bet: player.getBet(),
              winningChips: 0
            };
          })
          .sortBy(p => { return p.bet; })
          .value();
      })
      .value();
    const unOrderedResults = _(calculate(sorted)).flatten();
    return _(players).map(p => _(unOrderedResults).findWhere({player: p}));

  }
}

interface PotResult {
  player: Player;
  bet: number;
  winningChips: number;
}
