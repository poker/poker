import Ranks from './Ranks';
import Suits from './Suits';

export default class Card {

  constructor(private rank: Ranks, private suit: Suits) { }

  getRank() { return this.rank; }

  getSuit() { return this.suit; }

  prettyPrint() { return `${this.getRank().getShortName()}${this.getSuit().getUnicode()}`; }
}