import Hand from './Hand';
import Card from './Card';

export default class Player {

  private hand: Hand = new Hand();
  private betAmount: number = 0;
  private folded: boolean = false;

  constructor(private chips: number = 0) {}

  addCard(c: Card) {
    this.hand.addCard(c);
  }

  newHand(hand: Hand = new Hand()) {
    this.hand = hand;
    this.folded = false;
    this.betAmount = 0;
  }

  getHand() {
    return this.hand;
  }

  setChips(chips: number) { this.chips = chips; }
  getChips() { return this.chips; }
  addChips(n: number) { this.chips += n; }
  removeChips(n: number) { 
    this.chips -= n;
  }

  bet(chips: number) {
    this.chips -= chips;
    this.betAmount += chips;
  }

  getBet() { return this.betAmount; }

  payBet(chips: number) {
    chips = chips > this.betAmount ? this.betAmount : chips;
    this.betAmount -= chips;
    return chips; 
  }

  fold() {
    this.folded = true;
  }

  isFolded() {
    return this.folded;
  }

}