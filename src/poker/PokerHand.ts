import * as _ from 'underscore';
import Card from './Card';
import Ranks from './Ranks';
import Suits from './Suits';
import HandStrength from './HandStrength';

export default class PokerHand {

  constructor(private cards: Card[]) {
    if (cards.length != 5) throw new Error('Poker hand must have 5 cards.');
  }

  getCards() { return this.cards; }

  getScore() { return PokerHand.getScore(this); }

  getStrength() { return PokerHand.getStrength(this); }

  static getStrength(hand: PokerHand): HandStrength {
    const cards = hand.getCards(); 
    if (cards.length != 5) throw new Error('Hand must have 5 cards.');

    const histogram = _.chain(cards)
      .groupBy(card => { return card.getRank().getValue(); })
      .map(group => { return group.length; })
      .sortBy(count => -count)
      .value();

    if (histogram[0] == 4) 
      return HandStrength.Quads;

    if (histogram[0] == 3 && histogram[1] == 2) 
      return HandStrength.FullHouse;

    if (histogram[0] == 3) 
      return HandStrength.Set;

    if (histogram[0] == 2 && histogram[1] == 2) 
      return HandStrength.TwoPair;

    if (histogram.length == 4)
      return HandStrength.OnePair;

    var isFlush = false, isStraight = false;
    if (_.chain(cards)
      .groupBy(c => { return c.getSuit().getShortName(); })
      .keys().value().length == 1)
      isFlush = true;

    const sorted = _.sortBy(cards, (c) => { return c.getRank().getValue(); });
    if (_.last(sorted).getRank().getValue() - _.first(sorted).getRank().getValue() === 4)
      isStraight = true;
    else if (_.last(sorted).getRank() === Ranks.values.ACE && 
      _.chain(sorted).last(2).first().value().getRank() === Ranks.values.FIVE)
      isStraight = true;

    if (isStraight && isFlush)
      return HandStrength.StraightFlush;
    else if (isStraight)
      return HandStrength.Straight;
    else if (isFlush)
      return HandStrength.Flush;
    else
      return HandStrength.HighCard;
  }

  static getScore(hand: PokerHand) {
    const
      cards = hand.getCards(), 
      base = PokerHand.getStrength(hand),
      sorted = _(cards).sortBy(c => { return c.getRank().getValue(); });

    switch(base) {
      case HandStrength.StraightFlush: 
        return base + getStraightScore();
      case HandStrength.Quads:
        return base + sorted[2].getRank().getValue();
      case HandStrength.FullHouse:
        return base + sorted[2].getRank().getValue();
      case HandStrength.Flush:
        return base + getHighCardScore();
      case HandStrength.Straight:
        return base + getStraightScore();
      case HandStrength.Set:
        return base + sorted[2].getRank().getValue();
      case HandStrength.TwoPair:
        return base + getTwoPairScore();
      case HandStrength.OnePair:
        return base + getOnePairScore();
      case HandStrength.HighCard:
        return base + getHighCardScore();
      default:
        throw new Error('Is it really a hand?');
    }

    function getStraightScore() {
      if (sorted[4].getRank().getValue() - sorted[0].getRank().getValue() === 4)
        return sorted[4].getRank().getValue();
      else
        return sorted[3].getRank().getValue();
    }

    function getTwoPairScore() {
      return _.chain(cards).groupBy(card => { return card.getRank().getValue(); })
        .reduce((sum, cards, rankValue) => {
          rankValue = rankValue * 1;
          var score = rankValue;
          if (cards.length == 2) {
            score = rankValue * 14;
            // high pair
            if (sorted[3].getRank().getValue() === rankValue) score *= 14;
          }
          return sum + rankValue;
        }, 0)
        .value();
    }

    function getOnePairScore() {
      var pairValue;
      _.chain(cards)
        .groupBy(card => { return card.getRank().getValue(); })
        .some((cards, rankValue) => {
          if (cards.length == 2) {
            pairValue = rankValue;
            return true;
          }
        });
      return _.chain(sorted)
        .reject(card => card.getRank().getValue() == pairValue)
        .reduce((score, card, index) => {
          return score + Math.pow(14, index) * card.getRank().getValue();
        }, Math.pow(14, 3) * pairValue)
        .value();
    }

    function getHighCardScore() {
      return _(sorted).reduce((score, card, index) => {
        return score + Math.pow(14, index) * card.getRank().getValue();
      }, 0);
    }

  }
}
