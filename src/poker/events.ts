import Hand from './Hand';
import Player from './Player';

interface NewHand {
  players: Player[],
  button: number,
  blind: number
}

function NewHand(players: Player[], button: number, blind: number): NewHand {
  return {
    players: players,
    button: button,
    blind: blind
  }
}

interface FoldSuccess {}

interface FoldSuccessBroadcast {} 

interface HandEnd { winningChips: number[] }

function HandEnd(winningChips: number[]): HandEnd {
  return {
    winningChips: winningChips
  };
}

export {
  NewHand,
  FoldSuccess,
  FoldSuccessBroadcast,
  HandEnd
};