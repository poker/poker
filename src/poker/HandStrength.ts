enum HandStrength {
  StraightFlush = 8000000,
  Quads = 7000000,
  FullHouse = 6000000,
  Flush = 5000000,
  Straight = 4000000,
  Set = 3000000,
  TwoPair = 2000000,
  OnePair = 1000000,
  HighCard = 0
};

export { HandStrength as default};
