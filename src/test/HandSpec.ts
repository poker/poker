/// <reference path="../../typings/chai/chai.d.ts" />
/// <reference path="../../typings/mocha/mocha.d.ts" />
/// <reference path="../../typings/underscore/underscore.d.ts" />

import { expect } from 'chai';

import Hand from '../poker/Hand';
import Card from '../poker/Card';
import HandStrength from '../poker/HandStrength';
import Ranks from '../poker/Ranks';
import Suits from '../poker/Suits';

describe('Pick best hand from 7 stub', () => {

  it('test 1', () => {
    var hand = new Hand([
      new Card(Ranks.values.SEVEN, Suits.values.CLUB),
      new Card(Ranks.values.SEVEN, Suits.values.DIAMOND),
      new Card(Ranks.values.QUEEN, Suits.values.DIAMOND),
      new Card(Ranks.values.JACK, Suits.values.DIAMOND),
      new Card(Ranks.values.SEVEN, Suits.values.HEART),
      new Card(Ranks.values.JACK, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.HEART)
    ]);
    expect(hand.getBestPokerHand().getStrength()).to.eq(HandStrength.FullHouse);
  });
  
  it('test 2', () => {
    var hand = new Hand([
      new Card(Ranks.values.EIGHT, Suits.values.HEART),
      new Card(Ranks.values.FIVE, Suits.values.HEART),
      new Card(Ranks.values.SIX, Suits.values.SPADE),
      new Card(Ranks.values.THREE, Suits.values.HEART),
      new Card(Ranks.values.TEN, Suits.values.SPADE),
      new Card(Ranks.values.EIGHT, Suits.values.CLUB),
      new Card(Ranks.values.KING, Suits.values.SPADE)
    ]);
    expect(hand.getBestPokerHand().getStrength()).to.eq(HandStrength.OnePair);
  });
});
