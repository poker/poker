import { onConnection } from './io';
import * as db from './db';

import Table from '../models/Table';
import User from '../models/User';

import * as sv from './messages/lobby';
import * as cli from '../client/messages/lobby';

const lobbyId = 'lobby';

onConnection((socket: SocketIO.Socket) => {

  socket.on('JoinLobby', (data: cli.JoinLobby) => {
    const user = new User(socket, data.playerName);

    db.addUser(user);

    socket.join(lobbyId);

    socket.emit('JoinLobbySuccess', sv.JoinLobbySuccess(user.getTableChips(), db.getTables()));
  });

  socket.on('CreateTable', (data: cli.CreateTable) => {
    const table = new Table(data.blind);
    db.addTable(table);

    socket.emit('CreateTableSuccess', sv.CreateTableSuccess(table));
    broadcast(socket).emit('CreateTableBroadcast', sv.CreateTableBroadcast(table));
  });

  socket.on('LeaveLobby', () => {
    socket.leave(lobbyId);
    socket.emit('LeaveLobbySuccess');
  })
});

export function broadcast(socket: SocketIO.Socket) {
  return socket.broadcast.to(lobbyId);
}