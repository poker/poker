import * as _ from 'underscore';

import { onConnection, io } from './io';
import * as db from './db';

import Table from '../models/Table';
import Poker from '../poker/Poker';

import * as tb from './messages/table';
import * as cli from '../client/messages/table';
import * as pk from '../poker/events';

import { broadcast as broadcastLobby } from './lobby';

onConnection((socket: SocketIO.Socket) => {
  function checkAuthenticatedEvent(socket) {
    const table = db.getSocketTable(socket);
    if (table.getCurrUser().getSocket().id != socket.id)
      throw new Error('Unauthorized action');
  }

  socket.on('JoinTable', (data: cli.JoinTable) => {
    const table = db.getTable(data.tableId);
    const user = db.getUserWithSocket(socket);

    table.addPlayer(user);
    socket.join(table.id);

    socket.emit('JoinTableSuccess', tb.JoinTableSuccess(table));
    table.broadcast(socket).emit('JoinTableBroadcast', tb.JoinTableBroadcast(user));
    broadcastLobby(socket).emit('JoinTableLobby', tb.JoinTableLobby(table));
  });

  socket.on('StartPoker', () => {
    const table = db.getSocketTable(socket);

    const poker = table.getPoker();
    poker.on('NewHand', (data: pk.NewHand) => {
      _(table.getUsers()).each((user, index) => {
        const player = data.players[index];
        user.setTableChips(player.getChips());
        user.getSocket().emit('NewHand', tb.NewHand(player.getHand(), data.button, data.blind))
      });
    });
    poker.newHand();

    poker.on('HandEnd', (data: pk.HandEnd) => {
      const users = table.getUsers();

      _(data.winningChips).each((chips, i) => {
        users[i].addTableChips(chips);
      });
      table.broadcastAll(io).emit('HandEnd', tb.HandEnd(data.winningChips));

      if (_(users).filter(u => u.getTableChips() > 0).length > 1) {
        poker.newHand();
      }
    });

  });

  socket.on('Fold', () => {
    checkAuthenticatedEvent(socket);
    const table = db.getSocketTable(socket);
    const poker = table.getPoker();
    const foldPlayerName = table.getCurrUser().getName();

    socket.emit('FoldSuccess');
    table.broadcast(socket).emit('FoldBroadcast', tb.FoldBroadcast(foldPlayerName));

    poker.fold();
  });
});

