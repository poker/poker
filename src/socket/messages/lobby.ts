import * as _ from 'underscore';
import Table from '../../models/Table';

interface LobbyTable { id: string, blind: number, size: number }

function LobbyTable(t: Table): LobbyTable {
  return {
    id: t.id,
    blind: t.getBlind(),
    size: t.getUsers().length
  };
}

interface JoinLobbySuccess { tables: LobbyTable[], chips: number }

function JoinLobbySuccess(chips: number, tables: _.Dictionary<Table>): JoinLobbySuccess {
  return {
    chips: chips,
    tables: _.map(tables, t => LobbyTable(t))
  };
}

interface CreateTableSuccess { tableId: string }

function CreateTableSuccess(t: Table): CreateTableSuccess {
  return {
    tableId: t.getId()
  };
}

interface CreateTableBroadcast { table: LobbyTable }

function CreateTableBroadcast(t: Table): CreateTableBroadcast {
  return {
    table: LobbyTable(t)
  };
}

interface LeaveLobbySuccess {}
function LeaveLobbySuccess(): LeaveLobbySuccess { return; }

export {
  LobbyTable,
  JoinLobbySuccess,
  CreateTableSuccess,
  CreateTableBroadcast,
  LeaveLobbySuccess
};

export { JoinTableLobby } from './table';