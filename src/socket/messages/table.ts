import * as _ from 'underscore';
import User from '../../models/User';
import Table from '../../models/Table';

interface TablePlayer { id: string, name: string, chips: number }

function TablePlayer(user: User): TablePlayer {
  return {
    id: user.getId(),
    name: user.getName(),
    chips: user.getTableChips()
  };
}

interface JoinTableSuccess { players: TablePlayer[]; thisPlayerIndex: number; }

function JoinTableSuccess(table: Table): JoinTableSuccess {
  const users = table.getUsers();
  return {
    players: _(users).map(user => TablePlayer(user)),
    thisPlayerIndex: users.length - 1 
  };
}

interface JoinTableBroadcast { player: TablePlayer }

function JoinTableBroadcast(user: User): JoinTableBroadcast {
  return {
    player: TablePlayer(user)
  };
}

interface JoinTableLobby { tableId: string }

function JoinTableLobby(table: Table): JoinTableLobby {
  return {
    tableId: table.getId()
  };
}

interface NewHand { hand: any, button: number, blind: number }

function NewHand(hand: any, button: number, blind: number): NewHand {
  return {
    hand: hand,
    button: button,
    blind: blind
  };
}

interface FoldBroadcast { foldPlayerName: string }

function FoldBroadcast(foldPlayerName: string): FoldBroadcast {
  return {
    foldPlayerName: foldPlayerName
  };
}

interface HandEnd { winningChips: number[] }

function HandEnd(winningChips: number[]) {
  return {
    winningChips: winningChips
  };
}

export {
  TablePlayer,
  JoinTableSuccess,
  JoinTableBroadcast,
  JoinTableLobby,
  NewHand,
  FoldBroadcast,
  HandEnd
};