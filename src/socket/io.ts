/// <reference path="../../typings/socket.io/socket.io.d.ts"/>

import * as socket from 'socket.io';
import server from '../www';

export const io = socket(server);

export function onConnection(fn: (socket: SocketIO.Socket) => void) {
  io.on('connection', fn);
}
