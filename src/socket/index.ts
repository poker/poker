import { onConnection } from './io';

import './lobby';
import './table';

import * as db from './db';

onConnection(socket => {
  console.log('a user connected');
  socket.on('disconnect', () => {
    db.removeUserWithSocket(socket);
    console.log('user disconnected');
  });
});
