import * as _ from 'underscore';

import Table from '../models/Table';
import User from '../models/User';

const users: _.Dictionary<User> = {};
const tables: _.Dictionary<Table> = {};

export function addUser(u: User) { users[u.getId()] = u; }
export function getUserWithSocket(socket) {
  return _.find(users, user => user.getSocket() == socket);
}
export function removeUserWithSocket(socket) {
  const user = getUserWithSocket(socket);
  if (user) delete users[user.getId()];
}

export function addTable(t: Table) { tables[t.getId()] = t; }
export function getTable(id: string) { return tables[id]; }
export function getTables() { return tables; }
export function removeTable(id: string) {
  delete tables[id];
}
export function getSocketTable(socket) { 
  const user = getUserWithSocket(socket);
  const table = _.find(tables, t => _(t.getUsers()).contains(user));
  if (!table) throw new Error('Cannot find user table');
  return table;
}