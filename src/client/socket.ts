/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/socket.io-client.d.ts" />

import * as io from 'socket.io-client';
import * as angular from 'angular';
import * as _ from 'underscore';

const socket = io('http://localhost:3000');

const socketFactory = ($rootScope: ng.IRootScopeService) => {
  return function(scope: ng.IScope) {
    const deregisters = [];
    scope.$on('$destroy', () => deregisters.forEach(fn => fn()));

    const newSocket = _(socket).clone();
    newSocket.$on = function(event: string, fn: Function) {
      function wrapFn(...args) {
        $rootScope.$apply(() => {
          fn.apply(this, args);
        });
      }
      deregisters.push(() => socket.off(event, wrapFn));
      return socket.on(event, wrapFn);
    };
    newSocket.$once = function(event: string, fn: Function) {
      return socket.once(event, function(...args) {
        $rootScope.$apply(() => {
          fn.apply(this, args);
        });
      });
    };
    return newSocket;
  };
};
socketFactory.$inject = ['$rootScope'];

angular
  .module('socket', [])
  .factory('$socket', socketFactory);
