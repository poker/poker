interface JoinTable { tableId: string }

function JoinTable(tableId: string): JoinTable {
  return {
    tableId: tableId
  };
}

export {
  JoinTable
};