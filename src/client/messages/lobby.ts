interface JoinLobby { playerName: string; }

function JoinLobby(name: string): JoinLobby {
  return {
    playerName: name
  };
}

interface CreateTable { blind: number }

function CreateTable(blind: number): CreateTable {
  return {
    blind: blind
  };
}

interface LeaveLobby {}

function LeaveLobby(): LeaveLobby { return; }

export { JoinLobby, CreateTable, LeaveLobby };