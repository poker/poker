/// <reference path="../../typings/node/node.d.ts"/>
/// <reference path="../../typings/angularjs/angular.d.ts" />

import * as angular from 'angular';

import 'angular-ui-router';

import './login/main';
import './lobby/main';
import './table/main';

angular
  .module('poker', ['login', 'lobby', 'table'])
  .config(['$urlRouterProvider', $urlRouterProvider => {
    $urlRouterProvider.otherwise('/');
  }]);

angular.element(document).ready(() => {
  angular.bootstrap(document, ['poker']);
});
