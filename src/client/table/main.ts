/// <reference path="../../../typings/node/node.d.ts"/>
/// <reference path="../../../typings/angularjs/angular.d.ts" />

import 'angular-ui-router';
import '../socket';

import * as angular from 'angular';
import * as _ from 'underscore';
import * as fs from 'fs';

import * as sv from '../../socket/messages/table';
import * as cli from '../messages/table';

import Hand from '../../poker/Hand';
import Suits from '../../poker/Suits';
import Ranks from '../../poker/Ranks';
import Card from '../../poker/Card';

angular
  .module('table', ['ui.router'])
  .config(['$stateProvider', $stateProvider => {
    $stateProvider
    .state('table', {
      url: '/table',
      template: fs.readFileSync(`${__dirname}/table.html`),
      controller: Controller,
      controllerAs: 'ctrl',
      params: { tableId: null }
    })
  }]);

class Controller {
  socket: SocketIOClient.Socket;
  tabledId: string;

  players: Player[];
  thisPlayerIndex: number;
  hand: any;

  currPlayerIndex: number;

  logStr: string[] = [];

  static $inject = ['$state', '$stateParams', '$socket', '$scope'];

  constructor($state, $stateParams, $socket, $scope) {
    if (!$stateParams.tableId) {
      $state.go('login');
      return;
    }

    const socket: SocketIOClient.Socket = $socket($scope);
    this.socket = socket;

    this.tabledId = $stateParams.tableId;
    socket.emit('JoinTable', cli.JoinTable(this.tabledId));

    socket.$once('JoinTableSuccess', (data: sv.JoinTableSuccess) => {
      this.players = _(data.players).map(p => new Player(p));
      this.thisPlayerIndex = data.thisPlayerIndex;
      if (this.players.length >= 2) {
        socket.emit('StartPoker');
      }
    });

    socket.$once('JoinTableBroadcast', (data: sv.JoinTableBroadcast) => {
      this.players.push(new Player(data.player));

      this.log(`Player ${data.player.name} has joined the table.`);
    });

    socket.$on('NewHand', (data: sv.NewHand) => {
      _(this.players).each(p => {
        p.betAmount = 0;
      });
      this.currPlayerIndex = data.button;
      const small = this.getNextPlayer();
      const big = this.getNextPlayer(2);
      this.currPlayerIndex = this.getNextPlayerIndex(3);

      small.bet(data.blind / 2);
      big.bet(data.blind);

      this.hand = makeHand(data.hand);
      this.log(`New hand has begun.`);
    });

    socket.$on('FoldSuccess', () => { this.log('You have folded.'); });
    socket.$on('FoldBroadcast', (data: sv.FoldBroadcast) => { this.log(`${data.foldPlayerName} has folded.`); });

    socket.$on('HandEnd', (data: sv.HandEnd) => {
      _(data.winningChips).each((chips, i) => {
        const player = this.players[i];
        if (chips > 0) {
          this.log(`${player.name} won ${chips}.`);
          player.chips += chips;
        }
      });
    });
  }

  log(msg: string) {
    this.logStr.push(msg);
  }

  fold() {
    this.socket.emit('Fold');
  }

  getNextPlayerIndex(n: number = 1) { return (this.currPlayerIndex + n) % this.players.length; }
  getNextPlayer(n: number = 1) { return this.players[this.getNextPlayerIndex(n)]; }
}

class Player implements sv.TablePlayer {
  id: string;
  betAmount: number = 0;
  chips: number;
  name: string;
  constructor(data: sv.TablePlayer) {
    _.extend(this, data);
  }

  bet(chips: number) {
    this.betAmount += chips;
    this.chips -= chips;
  }
}

function makeCard(card: any) {
  const suit = _.find(Suits.values, s => s.getShortName() == card.suit.short);
  const rank = _.find(Ranks.values, r => r.getShortName() == card.rank.short);
  return new Card(rank, suit);
}

function makeHand(hand) {
  return new Hand(_(hand.cards).map(c => makeCard(c)));
}
