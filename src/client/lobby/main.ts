/// <reference path="../../../typings/node/node.d.ts"/>
/// <reference path="../../../typings/angularjs/angular.d.ts" />

import 'angular-ui-router';
import '../socket';

import * as angular from 'angular';
import * as fs from 'fs';
import * as _ from 'underscore';

import * as sv from '../../socket/messages/lobby';
import * as cli from '../messages/lobby';

angular
  .module('lobby', ['ui.router', 'socket'])
  .config(['$stateProvider', $stateProvider => {
    $stateProvider
      .state('lobby', {
        url: '/lobby',
        template: fs.readFileSync(`${__dirname}/lobby.html`),
        controller: Controller,
        controllerAs: 'ctrl',
        params: { name: null }
      });
  }]);
  
class Controller {
  myChips: number;
  tables: sv.LobbyTable[];
  socket: SocketIOClient.Socket;

  static $inject = ['$state', '$stateParams', '$socket', '$scope'];
  
  constructor(private $state, $stateParams, $socket, $scope) {
    if (!$stateParams.name) {
      $state.go('login');
      return;
    }

    const socket = this.socket = $socket($scope);
    socket.emit('JoinLobby', cli.JoinLobby($stateParams.name));

    socket.$once('JoinLobbySuccess', (data: sv.JoinLobbySuccess) => {
      this.tables = data.tables;
      this.myChips = data.chips;
    });
    
    socket.$once('CreateTableSuccess', (data: sv.CreateTableSuccess) => {
      socket.emit('LeaveLobby');
      socket.once('LeaveLobbySuccess', () => {
        this.goTable(data.tableId);
      });
    });

    socket.$on('CreateTableBroadcast', (data: sv.CreateTableBroadcast) => {
      this.tables.push(data.table);
    });

    socket.$on('JoinTableLobby', (data: sv.JoinTableLobby) => {
      _(this.tables).findWhere({id: data.tableId}).size++;
    });

  }

  createTable() {
    this.socket.emit('CreateTable', cli.CreateTable(2));
  }

  joinTable(table: sv.LobbyTable) {
    this.goTable(table.id);
  }

  goTable(tableId: string) {
    this.$state.go('table', { tableId: tableId });
  }
}
