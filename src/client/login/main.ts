/// <reference path="../../../typings/node/node.d.ts"/>
/// <reference path="../../../typings/angularjs/angular.d.ts" />

import 'angular-ui-router';

import * as angular from 'angular';
import * as fs from 'fs';

angular
  .module('login', ['ui.router'])
  .config(['$stateProvider', $stateProvider => {
    $stateProvider
    .state('login', {
      url: '/',
      template: fs.readFileSync(`${__dirname}/login.html`),
      controller: Controller,
      controllerAs: 'ctrl'
    })
  }]);
  
class Controller {
  name: string;
  form: ng.IFormController;

  static $inject = ['$state'];

  constructor(private $state) {}

  joinLobby() {
    this.form.$setSubmitted();
    if (this.form.$invalid) return;

    this.$state.go('lobby', { name: this.name });
  }
}
