/// <reference path="../../typings/socket.io-client/socket.io-client.d.ts" />

declare module SocketIOClient {
  interface Socket {
    $on(event: string, fn: Function): Socket;
    $once(event: string, fn: Function): Socket;
  }
}
